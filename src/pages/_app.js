import "styles/globals.css";
import "styles/index.css";
import Header from "components/header/Header.component";
import Footer from "components/footer/Footer.component";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function MyApp({ Component, pageProps }) {
  return (
    <div className="min-h-screen flex flex-col">
      <Header />
      <div className="flex-1">
        <Component {...pageProps} />
      </div>
      <Footer />
      <ToastContainer draggable={false} />
    </div>
  );
}

export default MyApp;
