import React from "react";
import Link from "next/link";

import Container from "components/container/Container.component";
import ProductItem from "components/productItem/ProductItem.component";
import { useStore } from "utils/store/store";
import CartSummary from "components/cartSummary/CartSummary.component";
import Head from "components/head/Head.component";
import { routes } from "utils/routes/routes";

const Cart = () => {
  const products = useStore(state => state.products);

  return (
    <Container>
      <Head title="Shopping cart" />

      <h3 className="text-3xl mb-12">Products cart summary</h3>

      {products.length > 0 ? (
        <div className="relative flex w-full justify-center flex-col md:justify-between md:flex-row">
          <div className="flex flex-col items-center gap-6 mb-14">
            {products.map(product => (
              <ProductItem key={product.id} product={product} isDelete={true} />
            ))}
          </div>

          <CartSummary />
        </div>
      ) : (
        <span>
          Cart is empty, add some{" "}
          <span className=" underline text-primary">
            <Link href={routes.products}>products</Link>
          </span>
        </span>
      )}
    </Container>
  );
};

export default Cart;
