import React from "react";
import Image from "next/image";

const NotFound = () => {
  return (
    <div className="w-full flex justify-center items-center flex-col py-12  sm:flex-row">
      <div>
        <Image src="/404.png" width="110" height="110" alt="error image" />
      </div>
      <div className="sm:ml-8">
        <h2 className="text-8xl">404</h2>
        <h3 className="text-xl mb-8">PAGE NOT FOUND</h3>
      </div>
    </div>
  );
};

export default NotFound;
