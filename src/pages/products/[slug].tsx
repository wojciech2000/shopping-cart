import React from "react";
import Image from "next/image";
import { IoIosArrowRoundBack } from "react-icons/io";
import Link from "next/link";

import { getProductQuery, getProductsQuery, initializeClient } from "utils/graphql/graphql";
import { Product as ProductType } from "utils/graphql/graphql.types";
import Container from "components/container/Container.component";
import { routes } from "utils/routes/routes";
import { useStore } from "utils/store/store";
import Button from "components/button/Button.comopnent";
import Head from "components/head/Head.component";

interface IProps {
  product: ProductType;
}

const Product: React.FC<IProps> = ({ product }) => {
  const { name, image, price, description, categories } = product;

  const addProduct = useStore(state => state.addProduct);

  return (
    <Container>
      <Head title={name} description={description} />

      <div className="flex w-full justify-end items-center mb-4">
        <Link href={routes.products}>
          <button className="flex items-center">
            <IoIosArrowRoundBack className="text-4xl" />
            <span>Back</span>
          </button>
        </Link>
      </div>
      <Image src={image} alt={name} layout="responsive" width={6} height={4} />
      <div className="mt-6 mb-14">
        <div className="pb-2 mb-2 border-b flex justify-between items-center">
          <h1 className="text-2xl ">{name}</h1>

          <Button
            onClick={() => addProduct(product)}
            className="w-32 h-9 rounded text-white bg-primary"
          >
            BUY
          </Button>
        </div>
        <h3 className="text-2xl text-primary">${price}</h3>
        <p className="mt-5 border rounded p-2">{description}</p>
        <div className="mt-4">
          {categories.map(({ id: categoryId, name: categoryName }) => (
            <span key={categoryId} className="mr-4 text-gray-500">
              {categoryName}
            </span>
          ))}
        </div>
      </div>
    </Container>
  );
};

export const getStaticPaths = async () => {
  const client = initializeClient();

  const result = await client.query({
    query: getProductsQuery,
  });

  const products: ProductType[] = result.data.products;

  const paths = products.map(product => ({
    params: { slug: product.slug },
  }));

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async context => {
  const { slug } = context.params;

  const client = initializeClient();

  const result = await client.query({
    query: getProductQuery,
    variables: { slug },
  });

  const [product]: [ProductType] = result.data.products;

  return {
    props: {
      product,
    },
  };
};

export default Product;
