import React, { useState } from "react";

import Container from "components/container/Container.component";
import { getProductsQuery, initializeClient } from "utils/graphql/graphql";
import { Product } from "utils/graphql/graphql.types";
import ProductItem from "components/productItem/ProductItem.component";
import Search from "components/search/Search.component";
import Head from "components/head/Head.component";

interface IProps {
  products: Product[];
}

const Products: React.FC<IProps> = ({ products }) => {
  const [searchValue, setSearchValue] = useState<string>("");

  const filteredProducts = () => {
    const filter = products.filter(product =>
      product.name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()),
    );

    return filter;
  };

  return (
    <Container>
      <Head title="Our cools products" />

      <Search setSearchValue={setSearchValue} />

      <main className="flex flex-wrap gap-6 justify-center items-center mb-14">
        {filteredProducts().map(product => (
          <ProductItem key={product.id} product={product} />
        ))}
      </main>
    </Container>
  );
};

export const getStaticProps = async () => {
  const client = initializeClient();

  const result = await client.query({
    query: getProductsQuery,
  });

  const products: Product[] = result.data.products;

  return {
    props: {
      products,
    },
  };
};

export default Products;
