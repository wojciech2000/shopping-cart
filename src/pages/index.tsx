import React from "react";
import Link from "next/link";
import Image from "next/image";

import Container from "components/container/Container.component";
import { routes } from "utils/routes/routes";
import Head from "components/head/Head.component";

const Home = () => {
  return (
    <Container>
      <Head />

      <div className="relative w-full h-60 sm:h-96">
        <Image src="/shopping.png" layout="fill" className="object-cover" alt="woman on shopping" />
        <div className="absolute text-gray-800 bg-white bg-opacity-80 rounded p-4 transform -mt-4 -translate-y-1/2 left-8 top-1/2 xs:left-10 md:left-44">
          <h2 className="text-2xl xs:text-4xl sm:text-6xl md:text-8xl text-primary">Hello</h2>
          <span className="block sm:text-2xl mb-2">Check out what we offer!</span>
          <Link href={routes.products}>
            <button className="px-6 py-2 text-xs border rounded-md bg-black text-white mt-2 w-auto inline-block cursor-pointer duration-300 transition-all hover:bg-white hover:text-black sm:text-base">
              Products
            </button>
          </Link>
        </div>
      </div>
    </Container>
  );
};

export default Home;
