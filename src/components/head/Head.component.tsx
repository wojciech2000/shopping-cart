import React from "react";
import HeadTag from "next/head";

interface IProps {
  title?: string;
  keywords?: string;
  description?: string;
}

const initialValues = {
  title: "Buy with us",
  keywords: "shopping, fashion, shops",
  description: "What are you waiting for? Start shopping!",
};

const Head: React.FC<IProps> = ({
  title = initialValues.title,
  keywords = initialValues.keywords,
  description = initialValues.description,
}) => {
  return (
    <HeadTag>
      <title>{title}</title>
      <meta name="keywords" content={keywords} />
      <meta name="description" content={description} />
      <meta charSet="utf-8" />
    </HeadTag>
  );
};

export default Head;
