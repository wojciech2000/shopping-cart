import React from "react";
import { Product } from "utils/graphql/graphql.types";
import Link from "next/link";
import { BsFillTrashFill } from "react-icons/bs";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";
import { useStore } from "utils/store/store";

interface IProps {
  product: Product;
  isDelete?: boolean;
}

const ProductItem: React.FC<IProps> = ({ product, isDelete }) => {
  const { id, image, name, price, slug } = product;

  const removeProduct = useStore(state => state.removeProduct);

  return (
    <div className="relative">
      <Link href={`/products/${slug}`}>
        <div className="w-80 sm:w-64 cursor-pointer">
          <LazyLoadImage effect="blur" src={image} alt={name} />
          <div className="flex flex-col">
            <span>{name}</span>
            <span className="text-primary">${price}</span>
          </div>
        </div>
      </Link>

      {isDelete && (
        <button onClick={() => removeProduct(id)} className="absolute right-2 top-2 z-999999">
          <BsFillTrashFill className="text-red-600" />
        </button>
      )}
    </div>
  );
};

export default ProductItem;
