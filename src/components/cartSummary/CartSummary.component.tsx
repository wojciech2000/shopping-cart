import React from "react";

import Container from "components/container/Container.component";
import { useStore } from "utils/store/store";
import Button from "components/button/Button.comopnent";

const CartSummary = () => {
  const { products, buyProducts } = useStore(state => state);

  const fullCost = products.reduce((acc, product) => {
    return acc + +product.price;
  }, 0);

  return (
    <>
      {/* MOBILE */}
      <div className="sticky bottom-0 border-t-2 bg-white 4 md:hidden">
        <div className="flex justify-between items-center py-4">
          <div>
            <p>
              <span className="text-gray-600 mr-2">Total cost:</span>
              <span className="text-primary text-2xl"> ${fullCost}</span>
            </p>

            <p>
              <span className="text-gray-600 mr-2">Total quantity:</span>
              <span className="text-primary text-2xl">{products.length}</span>
            </p>
          </div>

          <Button className="w-20 sm:w-32" onClick={buyProducts}>
            PAY
          </Button>
        </div>
      </div>

      {/* DESKTOP */}
      <div className="hidden md:block sticky top-32 border p-6 h-48">
        <div>
          <div className="mb-8">
            <p>
              <span className="text-gray-600 mr-2">Total cost:</span>
              <span className="text-primary text-2xl"> ${fullCost}</span>
            </p>

            <p>
              <span className="text-gray-600 mr-2">Total quantity:</span>
              <span className="text-primary text-2xl">{products.length}</span>
            </p>
          </div>

          <Button className="w-full" onClick={buyProducts}>
            PAY
          </Button>
        </div>
      </div>
    </>
  );
};

export default CartSummary;
