import Container from "components/container/Container.component";
import React from "react";

const Footer = () => {
  return (
    <div className="w-full bg-black mt-12">
      <Container>
        <div className="text-white py-3 text-center ">
          &copy; 2021 Buy with <span className="text-primary">us</span>
        </div>
      </Container>
    </div>
  );
};

export default Footer;
