import React, { useCallback, useEffect, useState } from "react";
import Link from "next/link";
import { GiHamburgerMenu } from "react-icons/gi";
import clsx from "clsx";

import { routes } from "utils/routes/routes";
import { useRef } from "react";

const navLinks = [
  { id: 2, title: "Home", href: routes.home },
  { id: 1, title: "Products", href: routes.products },
];

const Nav = () => {
  const navRef = useRef<HTMLDivElement>(null);

  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleClick = (event: Event) => {
    if (navRef.current?.contains(event?.target as Node)) {
      return;
    }

    setIsOpen(false);
  };

  useEffect(() => {
    window.addEventListener("click", handleClick);

    return () => {
      window.removeEventListener("click", handleClick);
    };
  }, []);

  return (
    <>
      {/* MOBIEL */}
      <div ref={navRef} className="sm:hidden">
        <GiHamburgerMenu
          fontSize="24px"
          className="text-2xl cursor-pointer"
          onClick={() => setIsOpen(is => !is)}
        />

        <nav
          className={clsx(
            "absolute flex-col -bottom-1 transform translate-y-full right-0 mt-8 border bg-white rounded p-3 z-10",
            isOpen ? "flex" : "hidden",
          )}
        >
          {navLinks.map(({ id, title, href }) => (
            <span key={id} onClick={() => setIsOpen(false)}>
              <Link href={href}>
                <a>{title}</a>
              </Link>
            </span>
          ))}
        </nav>
      </div>

      {/* TABLE / DESKTOP */}

      <nav className="hidden sm:block">
        {navLinks.map(({ id, title, href }) => (
          <span key={id} className="mr-4">
            <Link href={href}>
              <a>{title}</a>
            </Link>
          </span>
        ))}
      </nav>
    </>
  );
};

export default Nav;
