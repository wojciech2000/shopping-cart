import React from "react";
import { AiOutlineSearch } from "react-icons/ai";

interface IProps {
  setSearchValue: (value: string) => void;
}

const Search: React.FC<IProps> = ({ setSearchValue }) => {
  return (
    <div className="mb-14 w-full relative">
      <label htmlFor="search" className="cursor-pointer">
        <AiOutlineSearch className="text-3xl absolute left-2 top-1/2 transform -translate-y-1/2" />
      </label>
      <input
        type="text"
        id="search"
        className="bg-gray-100 rounded w-full py-2 pl-12 pr-4 focus:outline-primary"
        onChange={e => setSearchValue(e.currentTarget.value)}
      />
    </div>
  );
};

export default Search;
