import React, { useState } from "react";
import { FiShoppingCart } from "react-icons/fi";
import Link from "next/link";

import { useStore } from "utils/store/store";
import { routes } from "utils/routes/routes";
import { useEffect } from "react";

const ShoppingCartIcon = () => {
  const [productsAmount, setProductsAmount] = useState(0);
  const products = useStore(state => state.products);

  useEffect(() => {
    setProductsAmount(products.length);
  }, [products]);

  return (
    <Link href={routes.cart}>
      <div className="relative cursor-pointer">
        <div className="absolute -top-2 -left-2 bg-primary text-white w-5 h-5 text-xs rounded-full flex items-center justify-center">
          {productsAmount}
        </div>

        <FiShoppingCart className="mr-4 text-2xl cursor-pointer" />
      </div>
    </Link>
  );
};

export default ShoppingCartIcon;
