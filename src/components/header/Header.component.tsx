import React from "react";
import Link from "next/link";

import { routes } from "utils/routes/routes";
import Nav from "components/nav/Nav.component";
import Container from "components/container/Container.component";
import ShoppingCartIcon from "components/shoppingCartIcon/ShoppingCartIcon.component";

const Header = () => {
  return (
    <div className="w-full border-b-2 mb-12 flex justify-center sticky top-0 bg-white z-20">
      <Container>
        <div className="flex justify-between py-4 w-full">
          <h1 className="font-bold">
            <Link href={routes.home}>
              <a>
                BUY WITH <span className="text-primary">US</span>
              </a>
            </Link>
          </h1>

          <div className="flex items-center justify-center relative ">
            <ShoppingCartIcon />

            <Nav />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Header;
