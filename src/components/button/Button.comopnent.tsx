import React from "react";
import clsx from "clsx";

interface IProps {
  children: JSX.Element | string;
  className?: string;
  onClick: () => void;
}

const Button: React.FC<IProps> = ({ children, className, onClick }) => {
  return (
    <button
      onClick={() => onClick()}
      className={clsx(className, "px-4 py-1 rounded text-white bg-primary")}
    >
      {children}
    </button>
  );
};

export default Button;
