export interface Category {
  id: string;
  slug: string;
  name: string;
}

export interface Product {
  id: string;
  name: string;
  slug: string;
  image: string;
  price: string;
  description: string;
  categories: Category[];
}
