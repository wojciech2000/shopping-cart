import { ApolloClient, gql, InMemoryCache } from "@apollo/client";

export const initializeClient = () =>
  new ApolloClient({
    uri: "https://reasonapps-gql-api.vercel.app/api/graphql",
    cache: new InMemoryCache(),
  });

export const getProductsQuery = gql`
  query Products {
    products {
      id
      name
      slug
      image
      price
      description
      categories {
        id
        name
        slug
      }
    }
  }
`;

export const getProductQuery = gql`
  query Product($slug: String) {
    products(slug: $slug) {
      id
      name
      slug
      image
      price
      description
      categories {
        id
        name
        slug
      }
    }
  }
`;
