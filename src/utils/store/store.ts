import create from "zustand";
import { devtools, persist } from "zustand/middleware";
import { ProductState } from "utils/store/store.types";
import { Product } from "utils/graphql/graphql.types";
import { toast } from "react-toastify";

let store: any = set => ({
  //ATTRIBUTES
  products: [],

  //METHODS
  addProduct: (product: Product) =>
    set((state: ProductState) => {
      const isAlready = state.products.find(({ name }) => name === product.name);

      if (isAlready) {
        toast.warning("This product is already in the cart");

        return {
          products: state.products,
        };
      }

      toast.success("Product has been added");

      return {
        products: [...state.products, product],
      };
    }),

  removeProduct: (productId: string) =>
    set((state: ProductState) => {
      const filteredProducts = state.products.filter(
        product => product.id !== productId && product,
      );

      toast.success("Product has been deleted");

      return {
        products: filteredProducts,
      };
    }),

  buyProducts: () =>
    set((state: ProductState) => {
      if (state.products.length > 0) {
        toast.success("Products has been bought");
      } else {
        toast.warning("There is no products in the cart");
      }
      return {
        products: [],
      };
    }),
});

store = devtools(store);
store = persist(store, { name: "products" });

export const useStore = create<ProductState>(store);
