import { Product } from "utils/graphql/graphql.types";

export interface ProductState {
  products: Product[];
  addProduct: (product: Product) => void;
  removeProduct: (productId: string) => void;
  buyProducts: () => void;
}
